package com.libraryservice.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;

import com.libraryservice.dtos.Book;
import com.libraryservice.dtos.User;
import com.libraryservice.exception.BookAlreadyIssuedException;
import com.libraryservice.exception.BookNotIssuedToUserException;
import com.libraryservice.feignclients.BooksFeignClient;
import com.libraryservice.feignclients.UsersFeignClient;
import com.libraryservice.jpa.UserBooks;
import com.libraryservice.repository.UserBooksRepository;

public class LibrayServiceImplTest {
	private MockMvc mockMvc;

	@InjectMocks
	LibrayServiceImpl librayServiceImpl;

	@Mock
	private UsersFeignClient userFeignClient;

	@Mock
	private BooksFeignClient booksFeignClient;

	@Mock
	private LibrayServiceImpl libraryServ;

	@Mock
	private UserBooksRepository userBooksRepo;

	static Book book;
	static User user;
	static UserBooks userBook;
	static List<UserBooks> userBooks;

	@BeforeAll
	public static void setValues() {
		book = new Book(1l, "testBook1", "testdesc1");
		user = new User(1l, "testname", "testmail", "testpass");
		userBook = new UserBooks(1l, 1, 1);
		userBooks = new ArrayList<>();
		userBooks.add(userBook);
	}

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	void testAddBooktoUserException() {
		Mockito.when(userBooksRepo.findyByBookIdAndUserId(1, 1)).thenReturn(Optional.ofNullable(userBook));
		Assertions.assertThrows(BookAlreadyIssuedException.class, () -> librayServiceImpl.addBooktoUser(1, 1));

	}

	@Test
	void testAddBooktoUser() {
		Mockito.when(userBooksRepo.findyByBookIdAndUserId(1, 1)).thenReturn(Optional.ofNullable(null));
		Mockito.when(userBooksRepo.save(Mockito.any(UserBooks.class))).thenReturn(userBook);
		Assertions.assertEquals(userBook, librayServiceImpl.addBooktoUser(1, 1));

	}

	@Test
	void testReleaseBookToUserException() {
		Mockito.when(userBooksRepo.findyByBookIdAndUserId(1, 1)).thenReturn(Optional.ofNullable(null));
		Assertions.assertThrows(BookNotIssuedToUserException.class, () -> librayServiceImpl.releaseBookToUser(1, 1));
	}

	@Test
	void testReleaseBookToUser() {
		Mockito.when(userBooksRepo.findyByBookIdAndUserId(1, 1)).thenReturn(Optional.ofNullable(userBook));
		Mockito.doNothing().when(userBooksRepo).delete(Mockito.any(UserBooks.class));
		Assertions.assertEquals(userBook, librayServiceImpl.releaseBookToUser(1, 1));
	}

	@Test
	void testGetBooksByUser() {
		Mockito.when(userBooksRepo.findyByUserId(1l)).thenReturn(userBooks);
		Assertions.assertEquals(userBooks, librayServiceImpl.getBooksByUser(1l));
	}

	@Test
	void testDeleteUsersFromLibrary() {
		Mockito.when(userBooksRepo.deleteByUserId(1l)).thenReturn(1);
		librayServiceImpl.deleteUsersFromLibrary(1l);
		Mockito.verify(userBooksRepo).deleteByUserId(1);
	}

	@Test
	void testDeleteBooksFromLibrary() {
		Mockito.when(userBooksRepo.deleteByBookId(1)).thenReturn(1);
		librayServiceImpl.deleteBooksFromLibrary(1);
		Mockito.verify(userBooksRepo).deleteByBookId(1);
	}

}
