package com.libraryservice.userpact;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryservice.controller.Response;
import com.libraryservice.dtos.User;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;

@TestComponent
public class GetUsersPact {

	private ObjectMapper mapper=new ObjectMapper();


	RestTemplate restTemplate =new RestTemplate();


	@Rule
    public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("user-service-app", "localhost", 8092, this);

    @Pact(provider = "user-service-app", consumer = "library-service-app")
    public RequestResponsePact getUser(PactDslWithProvider builder) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        return builder.given("Get User").uponReceiving("GET User").path("/users/1").method("GET")
                .willRespondWith().status(200).headers(headers)
                .body("{\"message\":\"success\",\"body\":{\"id\":1,\"name\":\"raju\",\"email\":\"rajugangeru@gmail.com\",\"password\":\"epam123\"}}").toPact();
    }


    @Test
	@PactVerification
	public void getUserTest() throws Exception {
		ResponseEntity<Response> response = restTemplate.exchange(mockProvider.getConfig().url() + "/users/1",
				HttpMethod.GET, new HttpEntity<>(null, null), Response.class);
		byte[] json = mapper.writeValueAsBytes(response.getBody().getBody());
		User userObj = mapper.readValue(json, User.class);
        User user=new User(1l,"raju","rajugangeru@gmail.com","epam123");
		assertEquals(user, userObj);
	}

}
