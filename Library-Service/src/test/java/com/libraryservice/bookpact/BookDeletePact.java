package com.libraryservice.bookpact;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryservice.controller.Response;
import com.libraryservice.dtos.Book;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;

@TestComponent
public class BookDeletePact {


	private ObjectMapper mapper=new ObjectMapper();


	@Rule
	public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("book-service-app", "localhost", 8090, this);

	@Pact(provider = "book-service-app", consumer = "library-service-app")
	public RequestResponsePact deleteBook(PactDslWithProvider builder) {
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		return builder.given("delete a  Book").uponReceiving("delete REQUEST").path("/books/1").method("DELETE")
				.willRespondWith().status(200).headers(headers)
				.body("{\"message\":\"Successfully Deleted\",\"body\":{\"bookId\":1,\"name\":\"raju\",\"description\":\"testdesc\"}}")
				.toPact();
	}
	@Test
	@PactVerification
	public void deleteBookTest() throws Exception {
		Book book = new Book(1l, "raju", "testdesc");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Response> response = restTemplate.exchange(mockProvider.getConfig().url() + "/books/1",
				HttpMethod.DELETE, new HttpEntity<>(null, null), Response.class);
		byte[] json = mapper.writeValueAsBytes(response.getBody().getBody());
		Book bookObj = mapper.readValue(json, Book.class);
		assertEquals(book, bookObj);
	}

	@Test
	@PactVerification
	public void deleteBookStatusTest() throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Response> response = restTemplate.exchange(mockProvider.getConfig().url() + "/books/1",
				HttpMethod.DELETE, new HttpEntity<>(null, null), Response.class);
		assertEquals(200, response.getStatusCodeValue());
	}
}
