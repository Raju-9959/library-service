/*
package com.libraryservice.bookpact;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryservice.controller.Response;
import com.libraryservice.dtos.Book;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;

@TestComponent
public class BookUpdatePact {

	private ObjectMapper mapper=new ObjectMapper();


	RestTemplate restTemplate =new RestTemplate();

	@Rule
	public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("book-service-app", "localhost", 8090, this);

	@Pact(provider = "book-service-app", consumer = "library-service-app")
	public RequestResponsePact updateBook(PactDslWithProvider builder) {

		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		return builder.given("update a  Book").uponReceiving("Put REQUEST").path("/books/1").query("desc=testdesc&name=raju")
		.method("PUT").willRespondWith().status(200).headers(headers)
				.body("{\"message\":\"success\",\"body\":{\"bookId\":1,\"name\":\"raju\",\"description\":\"testdesc\"}}")
				.toPact();
	}


	@Test
	@PactVerification
	public void updateBookTest() throws Exception {
		Book book = new Book(1l, "raju", "testdesc");
		ResponseEntity<Response> response = restTemplate.exchange(mockProvider.getConfig().url() + "/books/1?desc=testdesc&name=raju",
				HttpMethod.PUT, new HttpEntity<>(null), Response.class);
		byte[] json = mapper.writeValueAsBytes(response.getBody().getBody());
		Book bookObj = mapper.readValue(json, Book.class);
		assertEquals(book, bookObj);
	}


}
*/
