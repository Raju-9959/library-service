package com.libraryservice.bookpact;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryservice.controller.Response;
import com.libraryservice.dtos.Book;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;

@TestComponent
public class BooksGetPact {

	private ObjectMapper mapper=new ObjectMapper();

	RestTemplate restTemplate =new RestTemplate();


	@Rule
	public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("book-service-app", "localhost", 8090, this);

	@Pact(provider = "book-service-app", consumer = "library-service-app")
	public RequestResponsePact getBooks(PactDslWithProvider builder) {
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		return builder.given("Get Books").uponReceiving("GET REQUEST").path("/books").method("GET")
				.willRespondWith().status(200).headers(headers)
				.body("{\"message\":\"Successful\",\"body\":[{\"bookId\":1,\"name\":\"raju\",\"description\":\"testdesc\"}]}").toPact();
	}


	@Test
	@PactVerification
	public void getBooksTest() throws Exception {
		Response response = restTemplate.getForObject(mockProvider.getConfig().url() + "/books", Response.class);
		Book book = new Book(1l, "raju", "testdesc");
		List<Book> books = new ArrayList<>();
		books.add(book);
		byte[] json = mapper.writeValueAsBytes(response.getBody());
		List<Book> booksResponse = mapper.readValue(json, new TypeReference<List<Book>>() {
		});
		assertEquals(books, booksResponse);
	}

}
