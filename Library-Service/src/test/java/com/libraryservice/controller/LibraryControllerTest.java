package com.libraryservice.controller;



import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.libraryservice.dtos.Book;
import com.libraryservice.dtos.User;
import com.libraryservice.feignclients.BooksFeignClient;
import com.libraryservice.feignclients.UsersFeignClient;
import com.libraryservice.services.LibrayServiceImpl;

class LibraryControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	LibraryController librarycontroller;

	@Mock
	private UsersFeignClient userFeignClient;

	@Mock
	private BooksFeignClient booksFeignClient;

	@Mock
	private LibrayServiceImpl libraryServ;

	private static Book book;
	private static User user;

	
	@BeforeAll
	public static void setValues() {

		book = new Book(1l, "testBook1", "testdesc1");
		user = new User(1l, "testname", "testmail", "testpass");

	}

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(librarycontroller).build();

	}

	@Test
	void testGetUsers() throws Exception {
		this.mockMvc.perform(get("/library/users")).andExpect(status().isOk());
	}

	@Test
	void testGetUsersVerfiy() {
		librarycontroller.getUsers();
		Mockito.verify(userFeignClient).getUsers();
	}

	@Test
	void testGetUser() throws Exception {
		this.mockMvc.perform(get("/library/users/1")).andExpect(status().isOk());
	}

	@Test
	void testGetUserVerify() {
		librarycontroller.getUser(Mockito.anyLong());
		Mockito.verify(userFeignClient).getUser(Mockito.anyLong());
	}

	@Test
	void testAddUser() {
		librarycontroller.addUser(user);
		Mockito.verify(userFeignClient).addUser(user);
	}

	@Test
	void testDeleteUser() throws Exception {
		doNothing().when(libraryServ).deleteUsersFromLibrary(1);
		this.mockMvc.perform(delete("/library/users/1")).andExpect(status().isOk());
	}

	@Test
	void testDeleteUserVerfiy() {
		librarycontroller.deleteUser(Mockito.anyLong());
		Mockito.verify(userFeignClient).deleteUser(Mockito.anyLong());
	}

	@Test
	void testUpdateUser() {
		librarycontroller.updateUser(user);
		Mockito.verify(userFeignClient).updateUser(user);
	}

	@Test
	void testGetBooks() throws Exception {
		this.mockMvc.perform(get("/library/books")).andExpect(status().isOk());
	}

	@Test
	void testGetBooksVerify() {
		librarycontroller.getBooks();
		Mockito.verify(booksFeignClient).getBooks();
	}

	

	@Test
	void testAddBookVerify() {
		librarycontroller.addBook(book);
		Mockito.verify(booksFeignClient).addBook(book);
	}

	@Test
	void testDeleteBook() throws Exception {
		doNothing().when(libraryServ).deleteBooksFromLibrary(1);
		this.mockMvc.perform(delete("/library/books/1")).andExpect(status().isOk());
	}

	@Test
	void testUpdateBookVerify() {
		librarycontroller.updateBook(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString());
		Mockito.verify(booksFeignClient).updateBook(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString());
	}

}
