package com.libraryservice.exception;

@SuppressWarnings("serial")
public class BookAlreadyIssuedException extends RuntimeException {
	public BookAlreadyIssuedException(String msg) {
		super(msg);
	}
}
