package com.libraryservice.exception;

@SuppressWarnings("serial")
public class BookNotIssuedToUserException extends RuntimeException {
	public BookNotIssuedToUserException(String msg) {
		super(msg);
	}
}
