package com.libraryservice.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.libraryservice.jpa.UserBooks;

@Repository
public interface UserBooksRepository extends JpaRepository<UserBooks, Long> {

	@Query("select e from UserBooks e where e.bookId=?1 and e.userId=?2")
	public Optional<UserBooks> findyByBookIdAndUserId(long bookId, long userId);

	@Query("select e from UserBooks e where  e.userId=?1")
	public List<UserBooks> findyByUserId(long userId);

    @Modifying
    @Transactional
	@Query("delete  from UserBooks e where  e.userId=?1")
	public Integer deleteByUserId(long userId);

    @Modifying
    @Transactional
	@Query("delete from UserBooks e where  e.bookId=?1")
	public Integer deleteByBookId(long bookId);
	

}
