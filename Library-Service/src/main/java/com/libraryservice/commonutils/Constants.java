package com.libraryservice.commonutils;

public enum Constants {
	SERVICE_UNAVAILABLE("The Service is presently unavailable"), SUCCESS("Successful"),
	BOOK_NOTFOUND("The book is not available"), USER_NOTFOUND("The user is not available");

	String msg;

	private Constants(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return this.msg;
	}
}
