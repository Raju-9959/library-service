package com.libraryservice.commonutils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.libraryservice.controller.Response;

public class CommonUtils {

	public static ResponseEntity<Response> serviceUnavilableResponse() {
		return new ResponseEntity<>(new Response(Constants.SERVICE_UNAVAILABLE.getMsg(), null),
				HttpStatus.SERVICE_UNAVAILABLE);
	}

}
