package com.libraryservice.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryservice.commonutils.Constants;
import com.libraryservice.dtos.AuthenticateDto;
import com.libraryservice.dtos.Book;
import com.libraryservice.dtos.User;
import com.libraryservice.dtos.UserDto;
import com.libraryservice.feignclients.BooksFeignClient;
import com.libraryservice.feignclients.UsersFeignClient;
import com.libraryservice.jpa.UserBooks;
import com.libraryservice.security.JwtUtil;
import com.libraryservice.security.UserDetailsServiceImpl;
import com.libraryservice.services.LibrayServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("library")
@Api(value = "Library Resource")
@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully added"),
		@ApiResponse(code = 200, message = "The request has been succeeded "),
		@ApiResponse(code = 500, message = "Internal Server error"),
		@ApiResponse(code = 404, message = "Thre resource is not found"),
		@ApiResponse(code = 400, message = "The requested resource is not allowed"),
		@ApiResponse(code = 204, message = "No resources are not available") })
public class LibraryController {
	private static Logger logger = LogManager.getLogger(LibraryController.class);

	@Autowired
	private UsersFeignClient userFeignClient;

	@Autowired
	private BooksFeignClient booksFeignClient;

	@Autowired
	private LibrayServiceImpl librayService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@PostMapping("authentication")
	@ApiOperation(value = "Getting jwt token for login", response = ResponseEntity.class)
	public ResponseEntity<Response> token(@RequestBody UserDto userDto) {
		ResponseEntity<Response> re = null;
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(userDto.getUsername(), userDto.getPassword()));
			final UserDetails userDetails = userDetailsService.loadUserByUsername(userDto.getUsername());
			AuthenticateDto authenticateDto = new AuthenticateDto(jwtTokenUtil.generateToken(userDetails),
					userDto.getUsername());
			re = new ResponseEntity<>(new Response(Constants.SUCCESS.getMsg(), authenticateDto), HttpStatus.OK);
		} catch (BadCredentialsException e) {
			re = new ResponseEntity<>(new Response("please check the credentials", null), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			re = new ResponseEntity<>(new Response(e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return re;
	}

	@GetMapping("users")
	@ApiOperation(value = "Getting all the users", response = ResponseEntity.class)
	public ResponseEntity<Response> getUsers() {
		logger.info("getUsers method called");
		return userFeignClient.getUsers();
	}

	@GetMapping("users/{id}")
	@ApiOperation(value = "Getting a User", response = ResponseEntity.class)
	public ResponseEntity<Response> getUser(@PathVariable long id) {
		logger.info("getUser method called");
		return userFeignClient.getUser(id);
	}

	@PostMapping("users")
	@ApiOperation(value = "Adding  a User", response = ResponseEntity.class)
	public ResponseEntity<Response> addUser(@RequestBody User user) {
		logger.info("addUser method called");
		return userFeignClient.addUser(user);

	}

	@DeleteMapping("users/{id}")
	@ApiOperation(value = "deleting a User", response = ResponseEntity.class)
	public ResponseEntity<Response> deleteUser(@PathVariable long id) {
		logger.info("deleteUser method called");
		librayService.deleteUsersFromLibrary(id);
		return userFeignClient.deleteUser(id);
	}

	@PutMapping("users")
	@ApiOperation(value = "Updating  a  User", response = ResponseEntity.class)
	public ResponseEntity<Response> updateUser(@RequestBody User user) {
		logger.info("updateUser method called");
		logger.info("user is" + user);
		return userFeignClient.updateUser(user);
	}

	@GetMapping("/books")
	@ApiOperation(value = "Getting all the books", response = ResponseEntity.class)
	public ResponseEntity<Response> getBooks() {
		logger.info("getBooks method called");
		return booksFeignClient.getBooks();
	}

	@GetMapping("books/{id}")
	@ApiOperation(value = "Getting a  book", response = ResponseEntity.class)
	public ResponseEntity<Response> getBook(@PathVariable long id) {
		logger.info("getBook method called");
		return booksFeignClient.getBook(id);
	}

	@PostMapping("books")
	@ApiOperation(value = "adding a Book", response = ResponseEntity.class)
	public ResponseEntity<Response> addBook(@RequestBody Book book) {
		logger.info("addBook method called");
		return booksFeignClient.addBook(book);
	}

	@DeleteMapping("books/{id}")
	@ApiOperation(value = "deleting a Book", response = ResponseEntity.class)
	public ResponseEntity<Response> deleteBook(@PathVariable long id) {
		logger.info("deleteBook method called");
		librayService.deleteBooksFromLibrary(id);
		return booksFeignClient.deleteBook(id);
	}

	@PutMapping("books/{id}")
	@ApiOperation(value = "updating a book", response = ResponseEntity.class)
	public ResponseEntity<Response> updateBook(@PathVariable long id, @RequestParam String desc,
			@RequestParam String name) {
		logger.info("updateBook method called");
		return booksFeignClient.updateBook(id, desc, name);
	}

	@GetMapping("users/{user_id}/books")
	@SuppressWarnings("unused")
	@ApiOperation(value = "Getting all the books of a user", response = ResponseEntity.class)
	public ResponseEntity<Response> getBooksByUser(@PathVariable("user_id") long userId) throws IOException {
		logger.info("issuetBookToUser method called");

		ResponseEntity<Response> userResponse = userFeignClient.getUser(userId);

		return new ResponseEntity<>(new Response(Constants.SUCCESS.getMsg(), librayService.getBooks(userId)),
				HttpStatus.CREATED);

	}

	@PostMapping("users/{user_id}/books/{book_id}")
	@SuppressWarnings("unused")
	@ApiOperation(value = "adding a book to the user", response = ResponseEntity.class)
	public ResponseEntity<Response> addBookToUser(@PathVariable("user_id") long userId,
			@PathVariable("book_id") long bookId) {
		logger.info("issuetBookToUser method called");

		ResponseEntity<Response> bookResponse = booksFeignClient.getBook(bookId);

		ResponseEntity<Response> userResponse = userFeignClient.getUser(userId);

		return new ResponseEntity<>(
				new Response(Constants.SUCCESS.getMsg(), librayService.addBooktoUser(userId, bookId)),
				HttpStatus.CREATED);

	}

	@DeleteMapping("users/{user_id}/books/{book_id}")
	@SuppressWarnings("unused")
	@ApiOperation(value = "releasing a book to user", response = ResponseEntity.class)
	public ResponseEntity<Response> releaseBookToUser(@PathVariable("user_id") long userId,
			@PathVariable("book_id") long bookId) {
		logger.info("releaseBookToUser method called");
		ResponseEntity<Response> bookResponse = booksFeignClient.getBook(bookId);

		ResponseEntity<Response> userResponse = userFeignClient.getUser(userId);

		return new ResponseEntity<>(
				new Response(Constants.SUCCESS.getMsg(), librayService.releaseBookToUser(bookId, userId)),
				HttpStatus.OK);

	}

}
