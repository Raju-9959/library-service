package com.libraryservice.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) {
		if (!username.equals("raju"))
			throw new UsernameNotFoundException("User Not found");
		return new UserDetailsModel(
				new LogInUser(1, "raju", "$2y$05$TWvN1uKlpdx85BTNuazzieG6zIrmj3QwrFphVFSxCXjMG0z8eRno.", "ADMIN"));
	}

}
