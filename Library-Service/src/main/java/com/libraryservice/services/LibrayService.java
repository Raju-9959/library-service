package com.libraryservice.services;

import com.libraryservice.jpa.UserBooks;


public interface LibrayService {

	public UserBooks addBooktoUser(long userId, long bookId) ;

	public UserBooks releaseBookToUser(long bookId, long userId) ;

}
