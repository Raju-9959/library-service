package com.libraryservice.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryservice.dtos.Book;
import com.libraryservice.exception.BookAlreadyIssuedException;
import com.libraryservice.exception.BookNotIssuedToUserException;
import com.libraryservice.feignclients.BooksFeignClient;
import com.libraryservice.jpa.UserBooks;
import com.libraryservice.repository.UserBooksRepository;

@Service
public class LibrayServiceImpl implements LibrayService {

	@Autowired
	private UserBooksRepository userBooksRepo;
	@Autowired
	private BooksFeignClient booksFeignClient;

	private static final Logger logger = LogManager.getLogger(LibrayServiceImpl.class);
	private ObjectMapper mapper = new ObjectMapper();

	public UserBooks addBooktoUser(long userId, long bookId) {
		logger.info("addBooktoUser method called");
		Optional<UserBooks> userBookOpt = userBooksRepo.findyByBookIdAndUserId(bookId, userId);
		if (userBookOpt.isPresent())
			throw new BookAlreadyIssuedException("Book is already issued to the user");
		UserBooks userBook = new UserBooks();
		userBook.setBookId(bookId);
		userBook.setUserId(userId);
		return userBooksRepo.save(userBook);
	}

	public UserBooks releaseBookToUser(long bookId, long userId) {
		logger.info("releaseBookToUser method called");
		Optional<UserBooks> userBookOpt = userBooksRepo.findyByBookIdAndUserId(bookId, userId);
		if (!userBookOpt.isPresent())
			throw new BookNotIssuedToUserException("The book :" + bookId + " is not issued to user :" + userId);
		userBooksRepo.delete(userBookOpt.get());
		return userBookOpt.get();
	}

	public List<UserBooks> getBooksByUser(long userId) {
		logger.info("getBooksByUser method called");
		return userBooksRepo.findyByUserId(userId);
	}

	public List<Book> getBooks(long userId) throws IOException {
		logger.info("getBooks method called");
		List<UserBooks> userBooks = getBooksByUser(userId);
		List<Book> books = new ArrayList<>();
		if (userBooks != null) {
			for (UserBooks userBook : userBooks) {
				byte[] json = mapper
						.writeValueAsBytes(booksFeignClient.getBook(userBook.getBookId()).getBody().getBody());
				Book book = mapper.readValue(json, Book.class);
				books.add(book);
			}
		}
		return books;
	}

	public void deleteUsersFromLibrary(long id) {
		logger.info("deleteUsersFromLibrary method called");
		userBooksRepo.deleteByUserId(id);
	}

	public void deleteBooksFromLibrary(long id) {
		logger.info("deleteBooksFromLibrary method called");
		userBooksRepo.deleteByBookId(id);
	}

}
