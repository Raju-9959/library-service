package com.libraryservice.feignclients;

import java.nio.ByteBuffer;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryservice.commonutils.CommonUtils;
import com.libraryservice.controller.Response;
import com.libraryservice.dtos.User;

import feign.FeignException;
import feign.hystrix.FallbackFactory;

@Component
public class UserClientFallback implements FallbackFactory<UsersFeignClient> {

	private static Logger logger = LogManager.getLogger(UserClientFallback.class);
	ResponseEntity<Response> responseEntity;

	@Override
	public UsersFeignClient create(Throwable cause) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			final Integer status = cause instanceof FeignException ? ((FeignException) cause).status() : 0;
			Optional<ByteBuffer> responseBody = cause instanceof FeignException
					? ((FeignException) cause).responseBody()
					: null;
			Response response = mapper.readValue(responseBody.get().array(), Response.class);
			logger.info("httpstatus  :" + status);
			responseEntity = new ResponseEntity<>(response, HttpStatus.valueOf(status));
		} catch (Exception e) {
                        logger.info(e.getMessage());
			e.printStackTrace();
			responseEntity = CommonUtils.serviceUnavilableResponse();
		}
		return new UsersFeignClient() {

			@Override
			public ResponseEntity<Response> getUsers() {
				return responseEntity;
			}

			@Override
			public ResponseEntity<Response> getUser(long id) {
				return responseEntity;
			}

			@Override
			public ResponseEntity<Response> addUser(User user) {
				return responseEntity;
			}

			@Override
			public ResponseEntity<Response> deleteUser(long id) {
				return responseEntity;
			}

			@Override
			public ResponseEntity<Response> updateUser(User user) {
				return responseEntity;
			}

		};

	}

}
