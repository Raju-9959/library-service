package com.libraryservice.feignclients;

import java.nio.ByteBuffer;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryservice.commonutils.CommonUtils;
import com.libraryservice.controller.Response;
import com.libraryservice.dtos.Book;

import feign.FeignException;
import feign.hystrix.FallbackFactory;

@Component
public class BookClientFallback implements FallbackFactory<BooksFeignClient> {

	private static Logger logger = LogManager.getLogger(BookClientFallback.class);
	ResponseEntity<Response> responseEntity;

	@Override
	public BooksFeignClient create(Throwable cause) {

		try {
			ObjectMapper mapper = new ObjectMapper();
			final Integer status = cause instanceof FeignException ? ((FeignException) cause).status() : 0;
			Optional<ByteBuffer> responseBody = cause instanceof FeignException
					? ((FeignException) cause).responseBody()
					: null;
			logger.info(responseBody);
			logger.info(responseBody.get());
			Response response = mapper.readValue(responseBody.get().array(), Response.class);
			logger.info("httpstatus  :" + status);
			responseEntity = new ResponseEntity<>(response, HttpStatus.valueOf(status));
		} catch (Exception e) {
                         logger.info(e.getMessage());
                         e.printStackTrace();
			responseEntity = CommonUtils.serviceUnavilableResponse();
		}

		return new BooksFeignClient() {

			@Override
			public ResponseEntity<Response> getBooks() {
				return responseEntity;
			}

			@Override
			public ResponseEntity<Response> getBook(@PathVariable("id") long id) {
				return responseEntity;
			}

			@Override
			public ResponseEntity<Response> addBook(Book book) {
				return responseEntity;
			}

			@Override
			public ResponseEntity<Response> deleteBook(@PathVariable("id") long id) {
				return responseEntity;
			}

			@Override
			public ResponseEntity<Response> updateBook(@PathVariable("id") long id, @RequestParam("desc") String desc,
					@RequestParam("name") String name) {
				return responseEntity;
			}
		};

	}

}
