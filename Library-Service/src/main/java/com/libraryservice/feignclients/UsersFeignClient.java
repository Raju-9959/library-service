package com.libraryservice.feignclients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.libraryservice.controller.Response;
import com.libraryservice.dtos.User;

@FeignClient(name = "user-service-app", fallbackFactory = UserClientFallback.class)
public interface UsersFeignClient {

	@GetMapping("/users")
	public ResponseEntity<Response> getUsers();

	@GetMapping("/users/{id}")
	public ResponseEntity<Response> getUser(@PathVariable long id);

	@PostMapping("/users")
	public ResponseEntity<Response> addUser(User user);

	@DeleteMapping("/users/{id}")
	public ResponseEntity<Response> deleteUser(@PathVariable long id);

	@PutMapping("/users")
	public ResponseEntity<Response> updateUser(@RequestBody User user);

}
