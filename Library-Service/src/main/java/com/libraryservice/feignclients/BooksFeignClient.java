package com.libraryservice.feignclients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.libraryservice.controller.Response;
import com.libraryservice.dtos.Book;

@FeignClient(name="book-service-app",fallbackFactory = BookClientFallback.class)
public interface BooksFeignClient {

	@GetMapping("/books")
	public ResponseEntity<Response> getBooks();

	@GetMapping("books/{id}")
	public ResponseEntity<Response> getBook(@PathVariable(value="id") long id) ;

	@PostMapping("books")
	public ResponseEntity<Response> addBook(@RequestBody Book book) ;

	@DeleteMapping("books/{id}")
	public ResponseEntity<Response> deleteBook(@PathVariable(value="id") long id) ;

	@PutMapping("books/{id}")
	public ResponseEntity<Response> updateBook(@PathVariable(value="id") long id, @RequestParam("desc") String desc,
			@RequestParam("name") String name) ;
}